# Facility Management

a system for managing apartment buildings and their rooms
* please see the end of this README for a short & sweet system specifications summary
* you will find the database facility_management.dal.model and uml diagram in the root project folder

## Project 3 Update

The observer pattern has mostly been implemented in the src --> model --> observer directory. The rest of the implementation can be found in MaintenanceService under the services package.

The bridge pattern is within the src --> model --> maintenance directory. Relevant interfaces and classes include AbstractRequest, Request, RequestType, PlumbingType, and ElectricalType. There is a sample implementation under view --> Main.

## Getting Started

clone the repo, and cd into the facility-management directory
then use the command "./gradlew run" for mac/linux or "gradlew run" on windows to run the program

if there is an issue with the java sdk, or it won't run, try building with ./gradlew build and then try running

* building will run the tests as well

running the program should allow you to type into the console to use the program

### Prerequisites

you'll want the java sdk. gradle should run without needing external resources.

## Running the tests

"./gradlew test" for mac/linux or "gradlew test" on windows

* ./gradlew build will also run tests

* note: if this does not work, please test within intelliJ

## Built With

* Gradle
* JUnit
* Heroku - Postgresql database

## Authors

* Riley Clarkson
* Zac Gallagher

# System Specifications


### Our Thoughts

although the system is not fully complete, many of the most important features work. we feel that due to our complete lack of experience with databases that this project was a great learning experience.

### Purpose & Scope

obviously this project is not for any other purpose than learning and testing. its scope only applies to those two areas and it thus very small.

## Features

* here we will note feature descriptions, priorities, and levels of completion (with proper grammar...)

### Adding Users

* Priority: 10
* Completion: 10
* All other features depend on being used by a user... so this feature was paramount. When the program is started, the user is prompted to make an account if they do not have one, and user information is stored in the database. Users can either be administrators or general users. User accounts are not actually verified due to the scope of this program - no passwords, just choose what you want to try out.


### Adding, Editing, & Deleting Buildings

* Priority: 10
* Completion: 10
* Again, all other features depend on the use/manipulation of buildings. Thus these features were essential. Administrator users can access these features through their menu exclusively.

### Listing Rooms / Room Information

* Priority: 9
* Completion: 10
* Listing rooms and their info was pretty vital to the project. To make a lease, a user would need to facility_management.dal.view the room information first. Admins would also need to see rooms to edit them or remove them. This feature is accessible from both admin and general user menus.

### Listing Buildings / Building Information

* Priority: 9
* Completion: 10
* Only administrators can facility_management.dal.view all builidings and their information. In the future, it would be a good idea to allow users to choose rooms by building or see buildings with openings as well. This feature is accessed from the admin menu.

### Create or Break Lease / Assign or Vacate Use

* Priority: 10
* Completion: 10
* This feature is kind of the point of this system! It creates a lease object and stores it in the database. It is accessible by general users. Users can then facility_management.dal.view this lease information when they want via their menu as well.

### Checking Use During Interval

* Priority: 7
* Completion: 5
* The code for this feature was written, but it does not work and has thus been commented out. Trying to use this feature would probably terminate the program. This incompletion was mostly due to lack of database experience (not knowing how to check for times between intervals in sql).

### Making Maintenance Requests

* Priority: 7
* Completion: 10
* General users have access to the maintenance menu, where they can choose to submit a request. This feature only creates the request - the request must be scheduled by further action.

### Scheduling Maintenance

* Priority: 6
* Completion: 10
* From the maintenance menu, a user can also choose to schedule their request by request ID. They are just prompted for the times they want to schedule for.

### Listing Maintenance Requests / Number of Requests / Cost

* Priority: 5
* Completion: 10
* General users are also able to see a list of maintenance requests and see how many requests are on file for the building. These requests will list cost and schedule, etc. when those orders have been generated (automatically) and scheduled.

### Missing Features

* Features we did not quite understand or have time to implement include:
	* list inspections
	* list actual usage / list usage
	* facility downtime

* All other features, we felt we were able to implement in one way or another with the features described above (although they may be called something else).







