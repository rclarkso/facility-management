package facility_management.DAO;

import facility_management.AppContext;
import facility_management.model.facility.Lease;
import facility_management.model.facility.Room;
import facility_management.model.use.GeneralUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.sql.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class LeaseDAOTest {
    ApplicationContext context = AppContext.getContext();

    private GeneralUser user;
    private Room room;
    private Date startDate;
    private Date endDate;
    private UUID leaseID;
    private String id;
    private Lease lease;
    private LeaseDAO leaseDAO;
    private RoomDAO roomDAO;

    @Before
    public void setUp() {
        user = (GeneralUser) context.getBean("generalUser");
        user.setUserID(UUID.randomUUID());
        startDate = Date.valueOf("2018-09-09");
        endDate = Date.valueOf("2018-10-10");
        leaseID = UUID.randomUUID();
        id = leaseID.toString();
        room = (Room) context.getBean("room");
        room.setCapacity(10);
        room.setBuildingID(UUID.randomUUID());
        room.setRoomID(UUID.randomUUID());
        room.setUnit(304);
        room.setRoomStatus(Room.RoomStatus.VACANT);
        room.setRent("1000");
        roomDAO = (RoomDAO) context.getBean("roomDAO");
        roomDAO.addRoom(room);
        lease = (Lease) context.getBean("lease");
        lease.setLeaseID(leaseID);
        lease.setRoom(room);
        lease.setUser(user);
        lease.setStartDate(startDate);
        lease.setEndDate(endDate);
        leaseDAO = (LeaseDAO) context.getBean("leaseDAO");
    }

    @After
    public void tearDown() {
        user = null;
        startDate = null;
        endDate = null;
        roomDAO = null;
        leaseID = null;
        id = null;
        room = null;
        lease = null;
        leaseDAO = null;

    }

    @Test
    public void testGetLease() {
        leaseDAO.createLease(lease);
        assertEquals(leaseDAO.getLease(lease.getLeaseID().toString()).getLeaseID(),lease.getLeaseID());
    }

}
