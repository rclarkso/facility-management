package facility_management.DAO;

import facility_management.AppContext;
import facility_management.model.use.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class UserDAOTest {
    ApplicationContext context = AppContext.getContext();

    private String firstName = "zac";
    private String lastName = "g";
    private UUID userID = UUID.randomUUID();
    private String uID = userID.toString();
    private String email = "z";
    private String phoneNumber = "2333";
    private User.UserType type = User.UserType.GENERAL;
    private User userTest;
    private UserDAO test;

    @Before
    public void setUp(){
        userTest = (User) context.getBean("user");
        userTest.setType(type);
        userTest.setUserID(userID);
        userTest.setFirstName(firstName);
        userTest.setLastName(lastName);
        userTest.setEmail(email);
        userTest.setPhoneNumber(phoneNumber);

        test = (UserDAO) context.getBean("userDAO");
    }
    @Test
    public void testGetUser() {
        test.addUser(userTest);
        assertEquals(test.getUser(uID).getUserID(), userTest.getUserID());

    }

    @After
    public void tearDown(){
        firstName = null;
        lastName = null;
        userID = null;
        uID = null;
        email = null;
        phoneNumber = null;
        type = null;
        userTest=null;
        test=null;

    }

}
