package facility_management.DAO;

import facility_management.AppContext;
import facility_management.model.maintenance.Request;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class MaintenanceDAOTest {
    ApplicationContext context = AppContext.getContext();

    private Timestamp timeOfRequest;
    private String issue = "Dirty";
    private UUID requestID = UUID.randomUUID();
    private String id = requestID.toString();
    private Request test;
    private MaintenanceDAO testDAO = new MaintenanceDAO();

    @Before
    public void setUp(){
        test = (Request) context.getBean("request");
        test.setTimeOfRequest(timeOfRequest);
        test.setIssue(issue);
        test.setRequestID(requestID);
        testDAO = (MaintenanceDAO) context.getBean("maintenanceDAO");
    }

    @Test
    public void testGetMaintenanceRequest() {
        testDAO.addMaintRequest(test);
        assertEquals(testDAO.getMaintenanceRequest(test.getRequestID().toString()).getRequestID(),test.getRequestID());
    }

    @After
    public void tearDown(){
        timeOfRequest=null;
        issue = null;
        requestID = null;
        id =null;
        test=null;
        testDAO = null;

    }

}
