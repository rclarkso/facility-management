package facility_management.DAO;

import facility_management.AppContext;
import facility_management.model.facility.Address;
import facility_management.model.facility.Building;
import facility_management.model.facility.Room;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class BuildingDAOTest {
    ApplicationContext context = AppContext.getContext();

    private Building building;
    private List<Room> rooms;
    private String buildingName;
    private Address address;
    private String phoneNumber;
    private String description;
    private UUID buildingID;
    private String id;
    private Building test;
    private BuildingDAO testDAO;

    @Before
    public void setUp() {

        testDAO = (BuildingDAO) context.getBean("buildingDAO");
        address = (Address) context.getBean("address");
        address.setCountry("country");
        address.setZip("zip");
        address.setStreetLineTwo("street");
        address.setStreetLineTwo("street");
        address.setCity("city");
        address.setState("state");
        buildingID = UUID.randomUUID();
        rooms = new ArrayList<>();
        Room room = (Room) context.getBean("room");
        room.setUnit(501);
        room.setCapacity(4);
        room.setRoomID(UUID.randomUUID());
        room.setBuildingID(buildingID);
        room.setRoomStatus(Room.RoomStatus.OCCUPIED);
        room.setRent("800");
        rooms.add(room);
        buildingName = "Loyola";
        phoneNumber = "1112223333";
        description = "a building";
        id = buildingID.toString();
        test = (Building) context.getBean("building");
        test.setDescription(description);
        test.setPhoneNumber(phoneNumber);
        test.setAddress(address);
        test.setRooms(rooms);
        test.setBuildingName(buildingName);
        test.setBuildingID(buildingID);
    }

    @After
    public void tearDown() {
        building = null;
        testDAO = null;
        buildingName = null;
        phoneNumber = null;
        description = null;
        buildingID = null;
        id = null;
    }

    @Test
    public void testAddAndGetBuilding() {
        testDAO.addBuilding(test);
        assertEquals(testDAO.getBuilding(id).getBuildingID(), test.getBuildingID());
    }

}
