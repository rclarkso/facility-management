package facility_management.model.maintenance;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

import static org.junit.Assert.assertEquals;


public class MaintenanceScheduleTest {
    ApplicationContext context = AppContext.getContext();

    private Timestamp startTime;
    private Timestamp endTime;
    private UUID scheduleID;
    private MaintenanceSchedule s;

    @Before
    public void setUp() {
        startTime = Timestamp.from(Instant.now());
        endTime = Timestamp.from(Instant.now());
        scheduleID = UUID.randomUUID();
        s = (MaintenanceSchedule) context.getBean("maintenanceSchedule");
        s.setEndTime(endTime);
        s.setScheduleID(scheduleID);
        s.setStartTime(startTime);
    }

    @After
    public void tearDown() {
        startTime = null;
        endTime = null;
        scheduleID = null;
        s = null;
    }

    @Test
    public void testGetScheduleID() {
        assertEquals(s.getScheduleID(), scheduleID);
    }

    @Test
    public void testGetStartTime() {
        assertEquals(s.getStartTime(), startTime);
    }

    @Test
    public void testGetEndTime() {
        assertEquals(s.getEndTime(), endTime);
    }

}
