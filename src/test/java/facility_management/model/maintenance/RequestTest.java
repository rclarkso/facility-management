package facility_management.model.maintenance;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class RequestTest {
    ApplicationContext context = AppContext.getContext();

    private Timestamp timeOfRequest;
    private String issue = "Dirty";
    private UUID requestID;
    private Order order;
    private Request test = (Request) context.getBean("request");

    @Before
    public void setUp(){
        order = test.getNewOrder();
        test.setIssue(issue);
        test.setTimeOfRequest(timeOfRequest);
        test.setRequestID(requestID);
    }
    @Test
    public void testGetNewOrder() {
        assertEquals(test.getNewOrder(), order);
    }

    @Test
    public void testGetRequestID() {
        assertEquals(test.getRequestID(), requestID);
    }

    @Test
    public void testGetTimeOfRequest() {
        assertEquals(test.getTimeOfRequest(), timeOfRequest);
    }

    @Test
    public void testGetIssue() {
        assertEquals(test.getIssue(), "Dirty");
    }
    @After
    public void tearDown(){
        timeOfRequest = null;
        issue = null;
        requestID = null;
        order = null;
        test = null;
    }

}
