package facility_management.model.maintenance;

import facility_management.AppContext;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class OrderTest {
    ApplicationContext context = AppContext.getContext();

    private Timestamp completed;
    private BigDecimal estimatedCost;
    private UUID orderID;
    private boolean resolved = true;
    private MaintenanceSchedule scheduledTime;
    private Order test = (Order) context.getBean("order");

    @Before
    public void setUp(){
        test.setOrderID(orderID);
        test.setCompleted(completed);
        test.setEstimatedCost(estimatedCost);
        test.setResolved(resolved);
        test.setScheduledTime(scheduledTime);
    }
    @Test
    public void testGetScheduledTime() {

        assertEquals(test.getScheduledTime(), scheduledTime);
    }

    @Test
    public void testGetCompleted() {
        assertEquals(test.getCompleted(), completed);
    }

    @Test
    public void testGetEstimatedCost() {
        assertEquals(test.getEstimatedCost(), estimatedCost);
    }

    @Test
    public void testGetOrderID() {
        assertEquals(test.getOrderID(), orderID);
    }

    @Test
    public void testIsResolved() {

        assertEquals(test.isResolved(), true);
    }

    @Test
    public void tearDown(){
        completed= null;
        estimatedCost=null;
        orderID=null;

        scheduledTime=null;
         test = null;


    }

}
