package facility_management.model.maintenance;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class MaintenanceLogTest {
    ApplicationContext context = AppContext.getContext();

    private List<MaintenanceSchedule> completedOrders;
    private List<MaintenanceSchedule> activeOrders;
    private MaintenanceLog test = (MaintenanceLog) context.getBean("maintenanceLog");

    @Before
    public void setUp(){
        test.setActiveOrders(activeOrders);
        test.setCompletedOrders(completedOrders);
    }
    @Test
    public void testGetCompletedOrders() {
        assertEquals(test.getCompletedOrders(), completedOrders);
    }

    @Test
    public void testGetActiveOrders() {

        assertEquals(test.getActiveOrders(), activeOrders);
    }

    @After
    public void tearDown(){
        completedOrders = null;
        activeOrders = null;
        test = null;
    }

}
