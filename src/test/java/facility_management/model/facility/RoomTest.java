package facility_management.model.facility;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class RoomTest {
    ApplicationContext context = AppContext.getContext();

    private Room room = (Room) context.getBean("room");

    @Before
    public void setUp(){
        room.setBuildingID(UUID.randomUUID());
        room.setCapacity(3);
        room.setFloor(1);
        room.setRent("900");
        room.setUnit(201);
        room.setRoomID(UUID.randomUUID());
        room.setRoomStatus(Room.RoomStatus.OCCUPIED);
    }

    @Test
    public void testGetCapacity() {


        assertEquals(room.getCapacity(), 3);

    }

    @Test
    public void testGetUnit() {
        assertEquals(room.getUnit(), 201);

    }

    @After
    public void tearDown(){
        room = null;
    }



}
