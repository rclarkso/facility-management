package facility_management.model.facility;

import facility_management.AppContext;
import facility_management.model.use.GeneralUser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.sql.Date;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class LeaseTest {
    ApplicationContext context = AppContext.getContext();

    private Room room = (Room) context.getBean("room");
    private GeneralUser user = (GeneralUser) context.getBean("generalUser");
    private Date startDate;
    private Date endDate;
    private UUID leaseID;
    private Lease l = (Lease) context.getBean("lease");

    @Before
    public void setUp(){
        l.setRoom(room);
        l.setEndDate(endDate);
        l.setStartDate(startDate);
        l.setUser(user);
        l.setLeaseID(leaseID);
    }

    @Test
    public void testGetUser() {
        assertEquals(l.getUser(), user);
    }

    @Test
    public void testGetStartDate() {
        assertEquals(l.getStartDate(), startDate);
    }

    @Test
    public void testGetEndDate() {
        assertEquals(l.getEndDate(), endDate);
    }

    @After
    public void tearDown(){
        room = (Room) context.getBean("room");
        user = (GeneralUser) context.getBean("generalUser");
        startDate=null;
        endDate=null;
        leaseID=null;
        l = null;

    }

}
