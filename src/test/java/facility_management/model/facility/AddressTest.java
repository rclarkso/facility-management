package facility_management.model.facility;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import static org.junit.Assert.assertEquals;

public class AddressTest {
    ApplicationContext context = AppContext.getContext();

    private Address address = (Address) context.getBean("address");

    @Before
    public void setUp(){
        address.setCity("Chicago");
        address.setStreetLineOne("Banana");
        address.setStreetLineTwo("Apple");
        address.setState("Illinois");
        address.setZip("60626");
        address.setCountry("USA");
    }

    @Test
    public void testGet() {
        assertEquals(address.getCity(), "Chicago");

        assertEquals(address.getStreetLineOne(), "Banana");

        assertEquals(address.getStreetLineTwo(), "Apple");

        assertEquals(address.getState(), "Illinois");

        assertEquals(address.getZip(), "60626");

        assertEquals(address.getCountry(), "USA");

    }

    @After
    public void tearDown(){

        context  = null;
        address = null;
    }

}
