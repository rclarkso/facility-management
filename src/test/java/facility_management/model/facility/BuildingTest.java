package facility_management.model.facility;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import static org.junit.Assert.assertEquals;

public class BuildingTest {

    ApplicationContext context = AppContext.getContext();
    private Building building = (Building) context.getBean("building");

    @Before
    public void setUp(){
        building.setBuildingName("Red");
        building.setPhoneNumber("22412345");
        building.setDescription("Dirty");
    }

    @Test
    public void testGet() {


        assertEquals(building.getBuildingName(), "Red");

        assertEquals(building.getPhoneNumber(), "22412345");

        assertEquals(building.getDescription(), "Dirty");


    }

    @After
    public void tearDown(){
        building = null;
        context = null;
    }


}
