package facility_management.model.use;

import facility_management.AppContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class UserTest {
    ApplicationContext context = AppContext.getContext();

    private User userTest =  (User) context.getBean("user");
    @Before
    public void setUp(){
        userTest.setEmail("email");
        userTest.setFirstName("Riley");
        userTest.setLastName("Test");
        userTest.setPhoneNumber("85239584");
        userTest.setUserID(UUID.randomUUID());
    }

    @Test
    public void testGet() {

        assertEquals(userTest.getFirstName(), "Riley");

        assertEquals(userTest.getLastName(), "Test");

        assertEquals(userTest.getEmail(), "email");

        assertEquals(userTest.getPhoneNumber(), "85239584");


    }
    @After
    public void tearDown(){
        context = null;
        userTest = null; 
    }


}
