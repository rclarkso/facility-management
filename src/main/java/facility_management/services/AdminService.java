package facility_management.services;

import facility_management.AppContext;
import facility_management.DAO.BuildingDAO;
import facility_management.DAO.RoomDAO;
import facility_management.DBHelper;
import facility_management.model.facility.Building;
import facility_management.model.facility.Room;
import org.springframework.context.ApplicationContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AdminService {
    ApplicationContext context = AppContext.getContext();

    public AdminService(){

    }

    public void removeBuilding(String buildingID){

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){

                String removeBuildingQuery = "DELETE FROM building WHERE buildingid='" + buildingID + "'";
                st.executeUpdate(removeBuildingQuery);

            }

            st.close();

        }  catch (SQLException se) {
            System.err.println("AdminService: Threw a SQLException deleting the building.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

    public void removeRoom(String roomID){

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){

                String removeRoomQuery = "DELETE FROM rooms WHERE roomid='" + roomID + "'";
                st.executeUpdate(removeRoomQuery);

            }

            st.close();

        }  catch (SQLException se) {
            System.err.println("AdminService: Threw a SQLException deleting the room.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

    public List<Building> listBuildings(){
        List<Building> buildings = new ArrayList<>();

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null) {

                String allBuildingIDS = "SELECT buildingid FROM building";
                ResultSet idResultSet = st.executeQuery(allBuildingIDS);
                BuildingDAO buildingDAO = (BuildingDAO) context.getBean("buildingDAO");

                while (idResultSet.next()){
                    Building building = buildingDAO.getBuilding(idResultSet.getString("buildingid"));
                    buildings.add(building);
                }

                idResultSet.close();
                st.close();

            }
        } catch (SQLException se) {
            System.err.println("AdminService: Threw a SQLException retrieving buildings.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return buildings;
    }

    public List<Room> listRooms(){
        List<Room> rooms = new ArrayList<>();

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null) {

                String allRoomIDs = "SELECT roomid FROM rooms";
                ResultSet idResultSet = st.executeQuery(allRoomIDs);
                RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");

                while (idResultSet.next()){
                    Room room = rDAO.getRoom(idResultSet.getString("roomid"));
                    rooms.add(room);
                }

                idResultSet.close();
                st.close();

            }
        } catch (SQLException se) {
            System.err.println("AdminService: Threw a SQLException retrieving buildings.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return rooms;
    }





}
