package facility_management.services;

import facility_management.AppContext;
import facility_management.DAO.RoomDAO;
import facility_management.DBHelper;
import facility_management.model.facility.Room;
import org.springframework.context.ApplicationContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GeneralService {
    ApplicationContext context = AppContext.getContext();

    public GeneralService(){

    }

    public List<Room> checkInterval(java.sql.Date one, java.sql.Date two){
        List<Room> rooms = new ArrayList<>();
        try {
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection()!=null) {
                String selectLease = "SELECT roomid FROM leases WHERE enddate < " + one;

                ResultSet leaseSet = st.executeQuery(selectLease);
                while (leaseSet.next()) {

                    RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");
                    Room room = rDAO.getRoom(leaseSet.getString("roomid"));

                    rooms.add(room);
                }
                return rooms;
            }
            else{
                System.out.println("Trouble connecting right now. Sorry!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    public void removeLease(String leaseID){
        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){

                String removeLease = "DELETE FROM leases WHERE leaseid='" + leaseID + "'";
                st.executeUpdate(removeLease);

                //TODO: alter room status
            }

            st.close();

        }  catch (SQLException se) {
            System.err.println("GeneralService: Threw a SQLException deleting the lease.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

    public List<Room> listRooms(){
        List<Room> rooms = new ArrayList<>();

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null) {

                String allRoomIDs = "SELECT roomid FROM rooms";
                ResultSet idResultSet = st.executeQuery(allRoomIDs);
                RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");

                while (idResultSet.next()){
                    Room room = rDAO.getRoom(idResultSet.getString("roomid"));
                    rooms.add(room);
                }

                idResultSet.close();
                st.close();

            }
        } catch (SQLException se) {
            System.err.println("AdminService: Threw a SQLException retrieving rooms.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return rooms;
    }
}
