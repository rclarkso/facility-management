package facility_management.services;

import facility_management.AppContext;
import facility_management.DAO.MaintenanceDAO;
import facility_management.DBHelper;
import facility_management.model.maintenance.Request;
import facility_management.model.observer.ConcreteSubject;
import facility_management.model.observer.Observer;
import org.springframework.context.ApplicationContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MaintenanceService {

    ApplicationContext context = AppContext.getContext();
    /** Instatiation for observer pattern to observe maintenance requests */
    ConcreteSubject subject;

    /**
     * For each request in the database, observers are notified of request statuses. In the request update method, when a request is updated the status is checked with notifyObservers()
     */
    public MaintenanceService(){
        subject = (ConcreteSubject) context.getBean("concretesubject");
        for (Request request : listRequests()){
            Observer observer = (Observer) context.getBean("concreteobserver");
            subject.attach(observer);
            observer.setSubject(subject);
            subject.setOrderResolved(request.getNewOrder().isResolved());
        }
    }

    /**
     * Observer - when a request is updated, observers are notified when the process is finished
     * @param request
     */
    public void updateRequest(Request request){
        try {
            Timestamp start = request.getNewOrder().getScheduledTime().getStartTime();
            Timestamp end = request.getNewOrder().getScheduledTime().getEndTime();


            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){

                String updateRequest = "UPDATE maintschedule SET starttime='" + start + "', endtime='" + end + "' WHERE scheduleid='" + request.getNewOrder().getScheduledTime().getScheduleID().toString() + "'";
                st.executeUpdate(updateRequest);


            }

            st.close();

        }  catch (SQLException se) {
            System.err.println("MaintenanceService: Threw a SQLException deleting the request.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        subject.notifyObservers();
    }

    public List<Request> listRequests(){
        List<Request> requests = new ArrayList<>();

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null) {

                String requestIDs = "SELECT requestid FROM maintrequests";
                ResultSet idResultSet = st.executeQuery(requestIDs);
                MaintenanceDAO mDAO = (MaintenanceDAO) context.getBean("maintenanceDAO");

                while (idResultSet.next()){
                    Request request = mDAO.getMaintenanceRequest(idResultSet.getString("requestid"));
                    requests.add(request);
                }

                idResultSet.close();
                st.close();

            }
        } catch (SQLException se) {
            System.err.println("MaintenanceService: Threw a SQLException retrieving requests.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
        return requests;
    }
}
