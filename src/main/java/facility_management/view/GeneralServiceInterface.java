package facility_management.view;

import facility_management.AppContext;
import facility_management.DAO.LeaseDAO;
import facility_management.DAO.RoomDAO;
import facility_management.DAO.UserDAO;
import facility_management.model.facility.Lease;
import facility_management.model.facility.Room;
import facility_management.model.use.GeneralUser;
import facility_management.services.GeneralService;
import org.springframework.context.ApplicationContext;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class GeneralServiceInterface {
    private Scanner scanner = new Scanner(System.in);
    ApplicationContext context = AppContext.getContext();

    public void selectGeneralService(){

        System.out.println("As a general user, you can\n1 - Check for vacant rooms during an interval\n2 - Create a Lease\n3 - View your lease\n4 - Break a Lease\n5 - List rooms\n6 - Go to the Maintenance Menu\nQ - Quit");

        switch(scanner.nextLine().toUpperCase()){
            case "1":
                /** doesn't work yet */
                checkAvailability();

                break;

            case "2":
                System.out.println("Please note that to create a lease, you will need the room ID. This can be found from the list rooms option. Do you have the room ID? Y or N");

                if(scanner.nextLine().toUpperCase().equals("Y")){
                    createLease();
                }
                else{
                    selectGeneralService();
                }
                break;

            case "3":
                System.out.println("Please note that to facility_management.dal.view your lease, you will need your lease ID. Do you have your lesea ID? Y or N");
                if(scanner.nextLine().toUpperCase().equals("Y")){
                    viewLease();
                    selectGeneralService();
                }
                else{
                    selectGeneralService();
                }

                break;

            case "4":
                System.out.println("Please note that to break your lease, you will need your lease ID. Do you have your lease ID? Y or N");
                if(scanner.nextLine().toUpperCase().equals("Y")){
                    breakLease();
                }
                else{
                    selectGeneralService();
                }
                break;

            case "5":

                GeneralService service =  (GeneralService) context.getBean("generalService");
                List<Room> rooms = service.listRooms();

                System.out.println(" ----- ROOMS ----- \n");
                for (Room r : rooms){
                    r.printInformation();
                    System.out.println("\n");
                }
                selectGeneralService();
                break;

            case "6":

                MaintenanceServiceInterface msi = (MaintenanceServiceInterface) context.getBean("maintenanceInterface");
                msi.selectMaintenanceService();

                break;

            case "Q":
                System.out.println("Terminating  . . .");
                System.exit(0);
                break;

            default:
                System.out.println("Input not recognized. Try again.");
                selectGeneralService();
        }

    }

    private void checkAvailability(){
        GeneralService service = (GeneralService) context.getBean("generalService");

        System.out.println("Date 1: IMPORTANT - FORMAT MUST BE yyyy-mm-dd ");
        Date dateOne = Date.valueOf(scanner.nextLine());

        System.out.println("Date 2: IMPORTANT - FORMAT MUST BE yyyy-mm-dd ");
        Date dateTwo = Date.valueOf(scanner.nextLine());

        List<Room> rooms = service.checkInterval(dateOne,dateTwo);

        System.out.println("\nThe following rooms are vacant between " + dateOne + " and " + dateTwo);

        for (Room r : rooms){
            r.printInformation();
        }

        System.out.println("\nTo see a complete list of rooms, please select that option from the main menu.");
        selectGeneralService();

    }

    private void createLease(){

        System.out.println("Please enter your user ID: ");
        String userID = scanner.nextLine();
        UserDAO uDAO = (UserDAO) context.getBean("userDAO");
        //idk what to do with spring for this one
        GeneralUser user = (GeneralUser) context.getBean("generalUser");
        user.setUser(uDAO.getUser(userID));

        System.out.println("Please enter the room ID: ");
        String roomID = scanner.nextLine();
        RoomDAO roomDAO = (RoomDAO) context.getBean("roomDAO");
        Room room = roomDAO.getRoom(roomID);

        System.out.println("Please enter the desired start date (FORMAT - yyyy-mm-dd) ");
        Date startDate = Date.valueOf(scanner.nextLine());

        LocalDate date = startDate.toLocalDate();
        java.sql.Date endDate = Date.valueOf(date.plusYears(1));

        System.out.println("The end date of your lease will be: " + endDate + ". Continue? Y or N" );

        if (scanner.nextLine().toUpperCase().equals("Y")){
            Lease lease = (Lease) context.getBean("lease");
            lease.setRoom(room);
            lease.setUser(user);
            lease.setStartDate(startDate);
            lease.setEndDate(endDate);
            lease.setLeaseID(UUID.randomUUID());
            LeaseDAO leaseDAO = (LeaseDAO) context.getBean("leaseDAO");
            leaseDAO.createLease(lease);
            System.out.println("Lease processing finished. Your lease ID is: " + lease.getLeaseID().toString() + ". Please keep the ID for your records. If there were errors, please try again.");
            selectGeneralService();
        }
        else{
            selectGeneralService();
        }
    }

    private Lease viewLease(){
        LeaseDAO lDAO = (LeaseDAO) context.getBean("leaseDAO");

        System.out.println("Please enter your lease ID: ");

        Lease lease = lDAO.getLease(scanner.nextLine());
        lease.printInformation();
        return lease;
    }

    private void breakLease(){
        Lease lease = viewLease();

        System.out.println("Are you sure you want to remove this lease with ID " + lease.getLeaseID().toString() + " ? Y or N");

        if (scanner.nextLine().toUpperCase().equals("Y")){
            GeneralService service = (GeneralService) context.getBean("generalService");
            service.removeLease(lease.getLeaseID().toString());
            System.out.println("Lease has been deleted. To review this change, choose the list buildings option from the main menu.");
        }

        selectGeneralService();
    }
}
