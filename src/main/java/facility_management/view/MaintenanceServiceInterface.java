package facility_management.view;

import facility_management.AppContext;
import facility_management.DAO.MaintenanceDAO;
import facility_management.model.maintenance.Request;
import facility_management.services.MaintenanceService;
import org.springframework.context.ApplicationContext;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class MaintenanceServiceInterface {

    Scanner scanner = new Scanner(System.in);
    ApplicationContext context = AppContext.getContext();

    public MaintenanceServiceInterface(){

    }

    public void selectMaintenanceService(){

        System.out.println("MAINTENANCE\n1 - Make a maintenance request\n2 - Schedule your maintenance request\n3 - View your request information\n4 - List requests\n5 - See number of building maintenance requests\n6 - Return to main menu\nQ - Quit");

        switch(scanner.nextLine().toUpperCase()){
            case "1":
                createMaintenanceRequest();
                break;

            case "2":
                System.out.println("Please note that to schedule a maintenance request, you will need the request ID. This can be found from the list requests option. Do you have the request ID? Y or N");

                if(scanner.nextLine().toUpperCase().equals("Y")){
                    scheduleMaintenance();
                }
                else{
                    selectMaintenanceService();
                }
                break;

            case "3":
                System.out.println("Please note that to facility_management.dal.view your request, you will need your request ID. Do you have your request ID? Y or N");
                if(scanner.nextLine().toUpperCase().equals("Y")){
                    viewRequest();
                    selectMaintenanceService();
                }
                else{
                    selectMaintenanceService();
                }

                break;

            case "4":
                System.out.println(" ----- MAINTENANCE REQUESTS ----- ");
                for (Request r : listRequests()){
                    r.printInformation();
                }
                selectMaintenanceService();
                break;

            case "5":

                System.out.println("Number of requests: " + listRequests().size());
                selectMaintenanceService();

                break;

            case "6":
                GeneralServiceInterface returnToMenu = (GeneralServiceInterface) context.getBean("generalInterface");
                returnToMenu.selectGeneralService();
                break;

            case "Q":
                System.out.println("Terminating  . . .");
                System.exit(0);
                break;

            default:
                System.out.println("Input not recognized. Try again.");
                selectMaintenanceService();
        }

    }

    public void createMaintenanceRequest(){
        MaintenanceDAO mDAO = (MaintenanceDAO) context.getBean("maintenanceDAO");

        Request request = (Request) context.getBean("request");

        System.out.println("Please enter a short summary of the issue: ");
        request.setIssue(scanner.nextLine());
        request.setTimeOfRequest(Timestamp.from(Instant.now()));
        request.setRequestID(UUID.randomUUID());

        mDAO.addMaintRequest(request);
        System.out.println("\nYour request has been submitted. Your request ID is: " + request.getRequestID().toString() + ". To schedule a time for this request, please select that option from the previous menu.");
        selectMaintenanceService();

    }

    public void scheduleMaintenance(){

        MaintenanceDAO maintenanceDAO = (MaintenanceDAO) context.getBean("maintenanceDAO");

        System.out.println("Please enter your request ID: ");

        Request request = maintenanceDAO.getMaintenanceRequest(scanner.nextLine());

        System.out.println("Please enter the time you'd like to have maintenance start: FORMAT yyyy-mm-dd hh:mm:ss ");
        request.getNewOrder().getScheduledTime().setStartTime(Timestamp.valueOf(scanner.nextLine()));

        System.out.println("Please enter the time you'd like to have maintenance done: FORMAT yyyy-mm-dd hh:mm:ss ");
        request.getNewOrder().getScheduledTime().setEndTime(Timestamp.valueOf(scanner.nextLine()));

        MaintenanceService service = (MaintenanceService) context.getBean("maintenanceService");
        service.updateRequest(request);

        maintenanceDAO.addMaintRequest(request);

        System.out.println("\nYour request has been scheduled.");
        selectMaintenanceService();

    }

    public void viewRequest(){
        MaintenanceDAO mDAO = (MaintenanceDAO) context.getBean("maintenanceDAO");

        System.out.println("Please enter your request ID: ");

        Request request = mDAO.getMaintenanceRequest(scanner.nextLine());

        request.printInformation();
    }

    public List<Request> listRequests(){

        MaintenanceService service = (MaintenanceService) context.getBean("maintenanceService");

        return service.listRequests();
    }
}
