package facility_management.view;

import facility_management.AppContext;
import facility_management.DAO.UserDAO;
import facility_management.model.use.AdminUser;
import facility_management.model.use.GeneralUser;
import facility_management.model.use.User;
import org.springframework.context.ApplicationContext;

import java.util.Scanner;
import java.util.UUID;

/**
 * User service interacts w/ user to create a user object
 */
public class GetUserInterface {

    Scanner scanner = new Scanner(System.in);
    ApplicationContext context = AppContext.getContext();

    public User.UserType getUser(){

        System.out.println("1: New User\n2: Existing User");

        if (scanner.nextLine().equals("1")){

            System.out.println("1: General User\n2: Administrator");

            String input = scanner.nextLine();

            if (input.equals("1")){

                GeneralUser generalUser = (GeneralUser) context.getBean("generalUser");
                return getUserInfo(generalUser, User.UserType.GENERAL);
            }
            else if (input.equals("2")){

                AdminUser adminUser = (AdminUser) context.getBean("adminUser");
                return getUserInfo(adminUser, User.UserType.ADMIN);
            }
        }
        else {

            System.out.println("Please enter your User ID, as was generated when you made an account.");

            String id = scanner.nextLine();

            UserDAO userDAO = (UserDAO) context.getBean("userDAO");

            return userDAO.getUser(id).getType();
        }

        return null;
    }

    public User.UserType getUserInfo(User user, User.UserType type){

        System.out.println("Email: ");
        user.setEmail(scanner.nextLine());

        System.out.println("Phone Number: ");
        user.setPhoneNumber(scanner.nextLine());

        System.out.println("First Name: ");
        user.setFirstName(scanner.nextLine());

        System.out.println("Last Name: ");
        user.setLastName(scanner.nextLine());

        user.setType(type);
        user.setUserID(UUID.randomUUID());
        UserDAO userDAO = (UserDAO) context.getBean("userDAO");
        userDAO.addUser(user);

        System.out.println("ID:" + user.getUserID().toString());

        return user.getType();
    }

}
