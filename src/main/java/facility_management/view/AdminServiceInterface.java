package facility_management.view;

import facility_management.AppContext;
import facility_management.DAO.BuildingDAO;
import facility_management.DAO.RoomDAO;
import facility_management.model.facility.Address;
import facility_management.model.facility.Building;
import facility_management.model.facility.Room;
import facility_management.services.AdminService;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class AdminServiceInterface {

    private Scanner scanner = new Scanner(System.in);
    ApplicationContext context = AppContext.getContext();

    public void selectAdminService(){

        System.out.println("As an administrator, you can:\n1 - Add a Building\n2 - Add a Room\n3 - Remove a Building\n4 - Remove a Room\n5 - Change a building's information\n6 - Change a room's information\n7 - List Buildings\n8 - List rooms\n9 - View specific room or building information\nQ - Quit");

        switch(scanner.nextLine().toUpperCase()){
            case "1":
                addBuilding(false);
                break;

            case "2":
                System.out.println("Please note that to add a room, you will need the building ID. Do you have the building ID? Y or N ");

                if (scanner.nextLine().toUpperCase().equals("Y")){
                    addRoom(false);
                }
                else{
                    selectAdminService();
                }
                break;

            case "3":
                System.out.println("Please note that to remove a building, you will need the building ID. Do you have the building ID? Y or N");

                if (scanner.nextLine().toUpperCase().equals("Y")){
                    removeBuilding();
                }
                else{
                    selectAdminService();
                }
                break;

            case "4":
                System.out.println("Please note that to remove a room, you will need the room ID. Do you have the room ID? Y or N");

                if (scanner.nextLine().toUpperCase().equals("Y")){
                    removeRoom();
                }
                else{
                    selectAdminService();
                }
                break;

            case "5":
                System.out.println("Please note that to edit a building, you will need the building ID. Do you have the room ID? Y or N");

                if (scanner.nextLine().toUpperCase().equals("Y")){
                    editBuilding();
                }
                else{
                    selectAdminService();
                }
                break;

            case "6":
                System.out.println("Please note that to edit a room, you will need the room ID. Do you have the room ID? Y or N");

                if (scanner.nextLine().toUpperCase().equals("Y")){
                    editRoom();
                }
                else{
                    selectAdminService();
                }
                break;

            case "7":
                listBuildings();

                break;

            case "8":
                listRooms();

                break;

            case "9":
                System.out.println("You will need your room or building ID to facility_management.dal.view information. Continue? Y or N");
                if (scanner.nextLine().toUpperCase().equals("Y")){
                    System.out.println("View Room or Building information? R or B");
                    String input = scanner.nextLine().toUpperCase();

                    if (input.equals("R")){
                        System.out.println("Please enter room ID: ");
                        roomCurrentInfo(scanner.nextLine());
                        selectAdminService();
                    }
                    else if (input.equals("B")){
                        System.out.println("Please enter building ID: ");
                        buildingCurrentInfo(scanner.nextLine());
                        selectAdminService();
                    }
                }
                else{
                    selectAdminService();
                }
                break;

            case "Q":
                System.out.println("Terminating  . . .");
                System.exit(0);
                break;

            default:
                System.out.println("Input not recognized. Try again.");
                selectAdminService();
        }
    }

    private Building addBuilding(boolean edit){

        Building building = (Building) context.getBean("building");

        System.out.println("Facility Name: ");
        building.setBuildingName(scanner.nextLine());

        System.out.println("Facility Phone Number: ");
        building.setPhoneNumber(scanner.nextLine());

        System.out.println("Short Description: ");
        building.setDescription(scanner.nextLine());

        Address address = (Address) context.getBean("address");

        System.out.println("Address - Street Line One: ");
        address.setStreetLineOne(scanner.nextLine());

        System.out.println("Address - Street Line Two: ");
        address.setStreetLineTwo(scanner.nextLine());

        System.out.println("Address - City: ");
        address.setCity(scanner.nextLine());

        System.out.println("Address - State: ");
        address.setState(scanner.nextLine());

        System.out.println("Address - Zip: ");
        address.setZip(scanner.nextLine());

        System.out.println("Address - Country: ");
        address.setCountry(scanner.nextLine());

        building.setAddress(address);

        /** only save the building if the user is adding (not editing) */
        if (!edit){
            building.setBuildingID(UUID.randomUUID());
            BuildingDAO buildingDAO = (BuildingDAO) context.getBean("buildingDAO");
            buildingDAO.addBuilding(building);
            System.out.println("Your building's ID is: " + building.getBuildingID().toString() + "\nPlease keep this for future account actions. Would you like to add rooms to your building? Y or N");
        }
        else{
            System.out.println("Press S to save changes.");
        }


        if (scanner.nextLine().toUpperCase().equals("Y")){
            addRoom(false);
        }
        else{
            selectAdminService();
        }
        return building;
    }

    private Room addRoom(boolean edit){

        Room room = (Room) context.getBean("room");

        System.out.println("Building ID: ");
        room.setBuildingID(UUID.fromString(scanner.nextLine()));

        System.out.println("Unit (numberical only): ");
        room.setUnit(Integer.parseInt(scanner.nextLine()));

        System.out.println("Floor (numerical only): ");
        room.setFloor(Integer.parseInt(scanner.nextLine()));

        System.out.println("Status: 'VACANT' or 'OCCUPIED' ");

        if (scanner.nextLine().toUpperCase().equals("VACANT")){
            room.setRoomStatus(Room.RoomStatus.VACANT);
        }
        else if(scanner.nextLine().toUpperCase().equals("OCCUPIED")){
            room.setRoomStatus(Room.RoomStatus.OCCUPIED);
        }

        System.out.println("Capacity (numerical only): ");
        room.setCapacity(Integer.parseInt(scanner.nextLine()));

        System.out.println("Rent (numerical only): ");
        room.setRent(scanner.nextLine());
        System.out.println(room.getRent());

        if (!edit){
            room.setRoomID(UUID.randomUUID());
            RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");
            rDAO.addRoom(room);
            System.out.println("Your room's ID is: " + room.getRoomID().toString() + "\nPlease keep this for future account actions. Would you like to add more rooms to a building? Y or N");
        }
        else{
            System.out.println("Press S to save changes.");
        }


        if (scanner.nextLine().toUpperCase().equals("Y")){
            addRoom(false);
        }
        else{
            selectAdminService();
        }
        return room;
    }

    private void removeBuilding(){

        System.out.println("Please enter building ID: ");
        String buildingID = scanner.nextLine();

        buildingCurrentInfo(buildingID);

        System.out.println("Are you sure you want to remove this building with ID " + buildingID + " ? Y or N");

        if (scanner.nextLine().toUpperCase().equals("Y")){
            AdminService adminService = (AdminService) context.getBean("adminService");
            adminService.removeBuilding(buildingID);
            System.out.println("Building has been deleted. To review this change, choose the list buildings option from the main menu.");
        }

        selectAdminService();
    }

    private void removeRoom(){

        System.out.println("Please enter room ID: ");
        String roomID = scanner.nextLine();

        roomCurrentInfo(roomID);

        System.out.println("Are you sure you want to remove this room with ID " + roomID + " ? Y or N");

        if (scanner.nextLine().toUpperCase().equals("Y")){
            AdminService adminService = (AdminService) context.getBean("adminService");
            adminService.removeRoom(roomID);
            System.out.println("Room has been deleted. To review this change, choose the list buildings option, and then facility_management.dal.view building information from the main menu.");
        }

        selectAdminService();
    }

    private void editBuilding(){

        System.out.println("Please enter building ID: ");

        String id = scanner.nextLine();
        buildingCurrentInfo(id);

        BuildingDAO buildingDAO = (BuildingDAO) context.getBean("buildingDAO");
        System.out.println("To edit building information, you must re-confirm any old information on file.");
        Building newBuilding = addBuilding(true);
        newBuilding.setBuildingID(UUID.fromString(id));
        AdminService aS = (AdminService) context.getBean("adminService");
        aS.removeBuilding(id);
        buildingDAO.addBuilding(newBuilding);

        System.out.println("Old information has been overwritten. Your building ID is still: " + newBuilding.getBuildingID().toString() + "\nPlease select list buildings option to review changes.");
        selectAdminService();
    }

    private void editRoom(){
        System.out.println("Please enter room ID: ");

        String id = scanner.nextLine();
        roomCurrentInfo(id);

        RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");
        System.out.println("To edit room information, you must re-confirm any old information on file.");
        Room newRoom = addRoom(true);
        newRoom.setRoomID(UUID.fromString(id));
        AdminService aS = (AdminService) context.getBean("adminService");
        aS.removeRoom(id);
        rDAO.addRoom(newRoom);

        System.out.println("Old information has been overwritten. Your room ID is still: " + newRoom.getRoomID().toString() + "\nPlease select list buildings option to review changes.");
        selectAdminService();
    }

    private void listBuildings(){
        AdminService service = (AdminService) context.getBean("adminService");
        List<Building> buildings = service.listBuildings();


        System.out.println(" ----- BUILDINGS ----- \n");

        if (buildings.size()==0){
            System.out.println("\nNo buildings at this time!");
        }
        for (Building b : buildings){
            System.out.println("\nBuilding Name: " + b.getBuildingName());
            System.out.println("Building ID: " + b.getBuildingID().toString() + "\n");
        }

        selectAdminService();

    }

    // 72c1aec1-f427-475f-a1e1-7b0225df615d

    private void listRooms(){
        AdminService service = (AdminService) context.getBean("adminService");
        List<Room> rooms = service.listRooms();

        System.out.println(" ----- ROOMS ----- \n");

        if (rooms.size()==0){
            System.out.println("\nNo rooms at this time!");
        }

        for (Room r : rooms){
            System.out.println("\nRoom Unit: " + r.getUnit());
            System.out.println("Room ID: " + r.getRoomID().toString());
        }

        selectAdminService();

    }

    private void buildingCurrentInfo(String buildingID){
        BuildingDAO buildingDAO = (BuildingDAO) context.getBean("buildingDAO");
        Building building = buildingDAO.getBuilding(buildingID);
        System.out.println(" ----- BUILDING INFORMATION ----- \n");
        building.printInformation();
    }

    private void roomCurrentInfo(String roomID){
        RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");
        Room room = rDAO.getRoom(roomID);
        System.out.println(" ----- ROOM INFORMATION ----- \n");
        room.printInformation();

    }


}
