package facility_management.view;

import facility_management.AppContext;
import facility_management.model.maintenance.*;
import facility_management.model.use.User;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

public class Main {


    public static void main(String[] args){

        System.out.println("***************** Application Context instantiated! ******************");

        GetUserInterface getUser = (GetUserInterface) AppContext.getContext().getBean("getUserInterface");

        User.UserType userType = getUser.getUser();

        if (userType.equals(User.UserType.GENERAL)){

            GeneralServiceInterface generalServiceInterface = (GeneralServiceInterface) AppContext.getContext().getBean("generalInterface");
            generalServiceInterface.selectGeneralService();

        }
        else {

            AdminServiceInterface adminServiceInterface = (AdminServiceInterface) AppContext.getContext().getBean("adminInterface");
            adminServiceInterface.selectAdminService();

        }


        /** Just an example of what we did with the bridge pattern. The PlumbingType() is the object of concern - it implements the RequestTYpe interface. */
        AbstractRequest aR = new Request(new PlumbingType(),new Order(),Timestamp.from(Instant.now()),"Leaky pipe", UUID.randomUUID());
        aR.applyType();
        AbstractRequest aRTwo = new Request(new ElectricalType(),new Order(),Timestamp.from(Instant.now()),"Breaker went out", UUID.randomUUID());
        aRTwo.applyType();

    }

}
