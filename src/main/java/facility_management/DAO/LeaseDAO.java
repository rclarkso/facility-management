package facility_management.DAO;

import facility_management.AppContext;
import facility_management.DBHelper;
import facility_management.model.facility.Lease;
import facility_management.model.use.GeneralUser;
import org.springframework.context.ApplicationContext;

import java.sql.*;
import java.util.UUID;

/**
 * class for facility_management.dal.view, such as making a lease, checking availabilities, etc.
 */
public class LeaseDAO {

    ApplicationContext context = AppContext.getContext();

    public LeaseDAO(){

    }

    /** retrieve a specific lease */
    public Lease getLease(String leaseID){

        try {

            /** Get the facility_management.dal.model.maintenance info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){
                String selectLease = "SELECT userid, startdate, enddate, roomid, rent FROM leases WHERE leaseid = '" + leaseID + "'";

                ResultSet leaseResultSet = st.executeQuery(selectLease);
                System.out.println("LeaseDAO: *************** Query " + selectLease);
                Lease lease = (Lease) context.getBean("lease");

                lease.setLeaseID(UUID.fromString(leaseID));

                while ( leaseResultSet.next() ) {

                    lease.setStartDate(leaseResultSet.getDate("startdate"));
                    lease.setEndDate(leaseResultSet.getDate("enddate"));
                    lease.setRent(leaseResultSet.getString("rent"));

                    /** do something special for setting the user */
                    UserDAO userDAO = (UserDAO) context.getBean("userDAO");
                    lease.setUser( (GeneralUser) userDAO.getUser(leaseResultSet.getString("userid")));
                    RoomDAO rDAO = (RoomDAO) context.getBean("roomDAO");
                    lease.setRoom(rDAO.getRoom(leaseResultSet.getString("roomid")));
                }
                leaseResultSet.close();
                st.close();

                return lease;

            }
            else{
                System.out.println("Trouble connecting right now. Sorry!");
            }
        }catch (SQLException ex){
            System.out.println("LeaseDAO: Error loading lease!");
            System.out.println(ex.getMessage());
        }
        return null;
    }

    /** create a lease */
    public void createLease(Lease lease){

        Connection connection = DBHelper.getConnection();
        PreparedStatement leasePreparedStatement = null;

        if (connection != null){

            try {
                System.out.println("LeaseDAO: *************** Inserting Lease ");
                String insertUser = "INSERT INTO leases (leaseid, userid, startdate, enddate, rent, roomid) VALUES(?, ?, ?, ?, ?, ?);";

                leasePreparedStatement = connection.prepareStatement(insertUser);
                leasePreparedStatement.setString(1,lease.getLeaseID().toString());
                leasePreparedStatement.setString(2,lease.getUser().getUserID().toString());
                leasePreparedStatement.setDate(3,lease.getStartDate());
                leasePreparedStatement.setDate(4,lease.getEndDate());
                leasePreparedStatement.setString(5,lease.getRent());
                leasePreparedStatement.setString(6,lease.getRoom().getRoomID().toString());
                leasePreparedStatement.executeUpdate();

                //TODO: alter room status


            } catch (SQLException ex){
                System.err.println("LeaseDAO: Threw a SQLException saving the lease object.");
                System.err.println(ex.getMessage());
            }
        }
        else{
            System.out.println("Having trouble connecting to the database, sorry.");
        }


    }
}
