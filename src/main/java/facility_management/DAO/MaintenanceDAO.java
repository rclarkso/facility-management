package facility_management.DAO;

import facility_management.AppContext;
import facility_management.DBHelper;
import facility_management.model.maintenance.MaintenanceSchedule;
import facility_management.model.maintenance.Order;
import facility_management.model.maintenance.Request;
import org.springframework.context.ApplicationContext;

import java.sql.*;
import java.util.UUID;

public class MaintenanceDAO {

    ApplicationContext context = AppContext.getContext();

    public MaintenanceDAO(){

    }

    public Request getMaintenanceRequest(String requestID){

        try {

            /** Get the facility_management.dal.model.maintenance info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){
                String selectMaintenanceReq = "SELECT timeofrequest, issue, orderid FROM maintrequests WHERE requestid = '" + requestID + "'";

                ResultSet maintResultSet = st.executeQuery(selectMaintenanceReq);
                System.out.println("MaintenanceDAO: *************** Query " + selectMaintenanceReq);

                Request request = (Request) context.getBean("request");
                request.setRequestID(UUID.fromString(requestID));

                while ( maintResultSet.next() ) {

                    request.setTimeOfRequest(maintResultSet.getTimestamp("timeofrequest"));
                    request.setIssue(maintResultSet.getString("issue"));

                    /** do something special for order */
                    request.getNewOrder().setOrderID(UUID.fromString(maintResultSet.getString("orderid")));

                }
                maintResultSet.close();

                /** query address DB where orderID is that of the request's */
                String selectOrderQuery = "SELECT timecompleted, cost, resolved, scheduleid FROM maintorders WHERE orderid = '" + request.getNewOrder().getOrderID().toString() + "'";
                ResultSet orderResultSet = st.executeQuery(selectOrderQuery);

                System.out.println("MaintenanceDAO: *************** Query " + selectOrderQuery);
                String scheduleID = "";
                while ( orderResultSet.next() ) {
                    request.getNewOrder().setCompleted(orderResultSet.getTimestamp("timecompleted"));
                    request.getNewOrder().setEstimatedCost(orderResultSet.getBigDecimal("cost"));
                    request.getNewOrder().setResolved(orderResultSet.getBoolean("resolved"));
                    request.getNewOrder().getScheduledTime().setScheduleID(UUID.fromString(orderResultSet.getString("scheduleid")));
                }

                /** query address DB where scheduleID is that of the order's */
                String selectScheduleQuery = "SELECT starttime, endtime FROM maintschedule WHERE scheduleid = '" + scheduleID + "'";
                ResultSet scheduleResultSet = st.executeQuery(selectScheduleQuery);

                System.out.println("MaintenanceDAO: *************** Query " + selectScheduleQuery);

                while ( scheduleResultSet.next() ) {
                    request.getNewOrder().getScheduledTime().setStartTime(scheduleResultSet.getTimestamp("starttime"));
                    request.getNewOrder().getScheduledTime().setEndTime(scheduleResultSet.getTimestamp("endtime"));
                }

                scheduleResultSet.close();
                orderResultSet.close();
                maintResultSet.close();

                st.close();

                return request;
            }
        }
        catch (SQLException se) {
            System.err.println("MaintenanceDAO: Threw a SQLException retrieving the facility_management.dal.model.maintenance request object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;
    }

    public void addMaintRequest(Request request){

        Connection connection = DBHelper.getConnection();
        PreparedStatement requestPreparedStatement = null, orderPreparedStatement = null, schedulePreparedStatment = null;

        if (connection!=null) {

            try {

                System.out.println("MaintenanceDAO: *************** Inserting Request ");

                String insertRequest = "INSERT INTO maintrequests (timeofrequest, issue, requestid, orderid) VALUES( ?, ?, ?, ?);";
                requestPreparedStatement = connection.prepareStatement(insertRequest);
                requestPreparedStatement.setTimestamp(1, request.getTimeOfRequest());
                requestPreparedStatement.setString(2, request.getIssue());
                requestPreparedStatement.setString(3, request.getRequestID().toString());
                requestPreparedStatement.setString(4,request.getNewOrder().getOrderID().toString());
                requestPreparedStatement.executeUpdate();

                System.out.println("MaintenanceDAO: *************** Inserting Order to be Processed ");

                Order order = request.getNewOrder();
                String insertOrder = "INSERT INTO maintorders (orderid, timecompleted, cost, resolved, scheduleid) VALUES (?, ?, ?, ?, ?);";
                orderPreparedStatement = connection.prepareStatement(insertOrder);
                orderPreparedStatement.setString(1, order.getOrderID().toString());
                orderPreparedStatement.setTimestamp(2, order.getCompleted());
                orderPreparedStatement.setBigDecimal(3, order.getEstimatedCost());
                orderPreparedStatement.setBoolean(4, order.isResolved());
                orderPreparedStatement.setString(5, order.getScheduledTime().getScheduleID().toString());
                orderPreparedStatement.executeUpdate();

                System.out.println("MaintenanceDAO: *************** Inserting Schedule for Order to be Scheduled ");

                MaintenanceSchedule schedule = order.getScheduledTime();
                String insertSchedule = "INSERT INTO maintschedule(starttime, endtime, scheduleid) VALUES (?, ?, ?);";
                schedulePreparedStatment = connection.prepareStatement(insertSchedule);
                schedulePreparedStatment.setTimestamp(1,schedule.getStartTime());
                schedulePreparedStatment.setTimestamp(2,schedule.getEndTime());
                schedulePreparedStatment.setString(3,schedule.getScheduleID().toString());
                schedulePreparedStatment.executeUpdate();



            } catch (SQLException ex) {
                System.out.println("Database Exception State: " + ex.getSQLState());
                System.out.println(ex.getMessage());
            } finally {

                try {
                    if (schedulePreparedStatment != null) {
                        schedulePreparedStatment.close();
                        orderPreparedStatement.close();
                        requestPreparedStatement.close();
                    }
                    connection.close();

                } catch (SQLException ex) {
                    System.err.println("MaintenanceDAO: Threw a SQLException saving the building object.");
                    System.err.println(ex.getMessage());
                }
            }

        }
        else{
            System.out.println("Having trouble connecting to the database, sorry.");
        }

    }

}
