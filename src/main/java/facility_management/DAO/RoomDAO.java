package facility_management.DAO;

import facility_management.AppContext;
import facility_management.DBHelper;
import facility_management.model.facility.Room;
import org.springframework.context.ApplicationContext;

import java.sql.*;
import java.util.UUID;

public class RoomDAO {
    ApplicationContext context = AppContext.getContext();

    public RoomDAO(){

    }

    /** retrieve a specific room */
    public Room getRoom(String roomID){
        System.out.println("RoomDAO: *************** Retrieving Room ");

        try {
            Statement st = DBHelper.getConnection().createStatement();

            if (st.getConnection() != null) {
                String selectRoom = "SELECT buildingid, capacity, unit, floor, status, rent FROM rooms WHERE roomid='" + roomID + "'";
                Room room = (Room) context.getBean("room");

                room.setRoomID(UUID.fromString(roomID));
                ResultSet roomResultSet = st.executeQuery(selectRoom);

                while (roomResultSet.next()) {

                    room.setCapacity(roomResultSet.getInt("capacity"));
                    room.setBuildingID(UUID.fromString(roomResultSet.getString("buildingid")));
                    room.setUnit(roomResultSet.getInt("unit"));
                    room.setFloor(roomResultSet.getInt("floor"));
                    room.setRent(roomResultSet.getString("rent"));

                    if (roomResultSet.getString("status").equals("VACANT")) {
                        room.setRoomStatus(Room.RoomStatus.VACANT);
                    } else {
                        room.setRoomStatus(Room.RoomStatus.OCCUPIED);
                    }
                }
                room.printInformation();
                return room;
            }
            else{
                System.out.println("Trouble connecting right now. Sorry!");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addRoom(Room room) {

        Connection connection = DBHelper.getConnection();
        PreparedStatement roomsPreparedSt = null;

        System.out.println("RoomDAO: *************** Inserting Room ");
        /** same thing for rooms (if any) */
        String insertRoom = "INSERT INTO rooms(roomid, buildingid, capacity, floor, status, unit, rent) VALUES (?, ?, ?, ?, ?, ?, ?);";

        if (connection != null) {
            try {

                roomsPreparedSt = connection.prepareStatement(insertRoom);
                roomsPreparedSt.setString(1, room.getRoomID().toString());
                roomsPreparedSt.setString(2, room.getBuildingID().toString());
                roomsPreparedSt.setInt(3, room.getCapacity());
                roomsPreparedSt.setInt(4, room.getFloor());
                roomsPreparedSt.setString(5, room.getRoomStatus().toString());
                roomsPreparedSt.setInt(6, room.getUnit());
                roomsPreparedSt.setString(7,room.getRent());
                roomsPreparedSt.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

                try {
                    if (roomsPreparedSt != null) {
                        roomsPreparedSt.close();
                    }
                    connection.close();

                } catch (SQLException ex) {
                    System.err.println("RoomDAO: Threw a SQLException saving the room object.");
                    System.err.println(ex.getMessage());
                }

            }
        }
    }
}
