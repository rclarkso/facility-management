package facility_management.DAO;

import facility_management.AppContext;
import facility_management.DBHelper;
import facility_management.model.use.AdminUser;
import facility_management.model.use.GeneralUser;
import facility_management.model.use.User;
import org.springframework.context.ApplicationContext;

import java.sql.*;
import java.util.UUID;

public class UserDAO {
    ApplicationContext context = AppContext.getContext();

    public UserDAO(){

    }

    public User getUser(String userID){

        try {
            Statement st = DBHelper.getConnection().createStatement();

            if (st.getConnection() != null) {

                String selectUserQuery = "SELECT type, email, firstname, lastname, phone, roomid FROM users WHERE userid = '" + userID + "'";
                ResultSet userResultSet = st.executeQuery(selectUserQuery);
                User user = (User) context.getBean("user");

                System.out.println("UserDAO: *************** Query " + selectUserQuery);
                AdminUser adminUser = null;
                GeneralUser generalUser = null;

                while (userResultSet.next()) {

                    user.setUserID(UUID.fromString(userID));
                    user.setEmail(userResultSet.getString("email"));
                    user.setFirstName(userResultSet.getString("firstname"));
                    user.setLastName(userResultSet.getString("lastname"));
                    user.setPhoneNumber(userResultSet.getString("phone"));
                    user.setFirstName(userResultSet.getString("firstname"));

                    /** create specific type of user via casting the general user object (based on the stored user type) */

                    if (userResultSet.getString("type").equals("GENERAL")) {
                        generalUser = (GeneralUser) context.getBean("generalUser");
                        generalUser.setUser(user);

                        generalUser.setType(User.UserType.GENERAL);
                        generalUser.setRoomID(UUID.fromString(userResultSet.getString("roomid")));

                    } else {
                        adminUser = (AdminUser) context.getBean("adminUser");
                        adminUser.setUser(user);
                        adminUser.setType(User.UserType.ADMIN);

                    }

                    /** return respective user based on which one was created */

                }
                userResultSet.close();
                st.close();

                return generalUser == null ? adminUser : generalUser;

            }
            else{
                System.out.println("Trouble connecting right now. Sorry!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }

    public void addUser(User user){
        Connection connection = DBHelper.getConnection();
        PreparedStatement userPreparedStatement = null;

        if (connection != null){

            try {
                System.out.println("UserDAO: *************** Inserting User ");
                String insertUser = "INSERT INTO users (userid, firstname, lastname, email, phone, type) VALUES( ?, ?, ?, ?, ?, ?);";

                userPreparedStatement = connection.prepareStatement(insertUser);
                userPreparedStatement.setString(1,user.getUserID().toString());
                userPreparedStatement.setString(2,user.getFirstName());
                userPreparedStatement.setString(3,user.getLastName());
                userPreparedStatement.setString(4,user.getEmail());
                userPreparedStatement.setString(5,user.getPhoneNumber());

                /** these must be done based on user type */

                if (user instanceof GeneralUser) {
                    GeneralUser genUser = (GeneralUser) user;
                    System.out.println("UserDAO: *************** General User -- Adding ");
                    userPreparedStatement.setString(6,"GENERAL");
                }
                else{
                    System.out.println("UserDAO: *************** Admin User -- Adding ");
                    userPreparedStatement.setString(6,"ADMIN");
                }


                userPreparedStatement.executeUpdate();


            } catch (SQLException ex){
                System.out.println("UserDAO: Threw a SQLException saving the user object.");
                System.out.println(ex.getMessage());
            }
        }
        else{
            System.out.println("Having trouble connecting to the database, sorry.");
        }

    }
}
