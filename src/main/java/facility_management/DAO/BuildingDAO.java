package facility_management.DAO;

import facility_management.AppContext;
import facility_management.DBHelper;
import facility_management.model.facility.Address;
import facility_management.model.facility.Building;
import facility_management.model.facility.Room;
import org.springframework.context.ApplicationContext;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Based off the customerDAO class as seen in the bookstore example project in class.
 */
public class BuildingDAO {

    ApplicationContext context = AppContext.getContext();

    public BuildingDAO(){

    }

    public Building getBuilding(String buildingID) {

        try {

            /** Get the building info */
            Statement st = DBHelper.getConnection().createStatement();
            if (st.getConnection() != null){
                String selectBuildingQuery = "SELECT buildingname, buildingphonenumber, buildingdescription FROM building WHERE buildingid = '" + buildingID + "'";

                ResultSet buildingResultSet = st.executeQuery(selectBuildingQuery);
                System.out.println("BuildingDAO: *************** Query " + selectBuildingQuery);


                Building building = (Building) context.getBean("building");
                building.setBuildingID(UUID.fromString(buildingID));

                String addressID = "";

                while ( buildingResultSet.next() ) {

                    building.setDescription(buildingResultSet.getString("buildingdescription"));
                    building.setBuildingName(buildingResultSet.getString("buildingname"));
                    building.setPhoneNumber(buildingResultSet.getString("buildingphonenumber"));

                }

                buildingResultSet.close();

                /** query address DB where addressID is that of the facility_management.dal.model.maintenance.facility's */
                String selectAddressQuery = "SELECT streetlineone, streetlinetwo, city, state, zip, country FROM addresses WHERE buildingid = '" + buildingID + "'";
                ResultSet addressResultSet = st.executeQuery(selectAddressQuery);
                Address address = (Address) context.getBean("address");

                System.out.println("BuildingDAO: *************** Query " + selectAddressQuery);

                while ( addressResultSet.next() ) {
                    address.setStreetLineOne(addressResultSet.getString("streetlineone"));
                    address.setStreetLineTwo(addressResultSet.getString("streetlinetwo"));
                    address.setCity(addressResultSet.getString("city"));
                    address.setState(addressResultSet.getString("state"));
                    address.setZip(addressResultSet.getString("zip"));
                    address.setCountry(addressResultSet.getString("country"));
                }

                building.setAddress(address);
                addressResultSet.close();

                /** query for rooms where the room has the building id in question */
                String selectRoomsQuery = "SELECT roomid, capacity, unit, floor, status FROM rooms WHERE buildingid='" + buildingID + "'";
                ResultSet roomResultSet = st.executeQuery(selectRoomsQuery);
                List<Room> rooms = new ArrayList<>();
                System.out.println("BuildingDAO: *************** Query " + selectRoomsQuery);

                while (roomResultSet.next()){
                    Room room = (Room) context.getBean("room");

                    room.setCapacity(roomResultSet.getInt("capacity"));
                    room.setRoomID(UUID.fromString(roomResultSet.getString("roomid")));
                    room.setUnit(roomResultSet.getInt("unit"));
                    room.setFloor(roomResultSet.getInt("floor"));

                    if (roomResultSet.getString("status").equals("VACANT")){
                        room.setRoomStatus(Room.RoomStatus.VACANT);
                    }
                    else{
                        room.setRoomStatus(Room.RoomStatus.OCCUPIED);
                    }

                    rooms.add(room);

                }

                building.setRooms(rooms);


                st.close();

                return building;
            }
        }
        catch (SQLException se) {
            System.err.println("BuildingDAO: Threw a SQLException retrieving the building object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;
    }


    public void addBuilding(Building building){

        Connection connection = DBHelper.getConnection();
        PreparedStatement buildingPreparedSt = null, addressPreparedSt = null, roomsPreparedSt = null;

        if (connection!=null) {

            try {
                System.out.println("BuildingDAO: *************** Inserting Building ");
                /** insert building object */
                String insertBuilding = "INSERT INTO building (buildingid, buildingdescription, buildingname, buildingphonenumber) VALUES(?, ?, ?, ?);";
                buildingPreparedSt = connection.prepareStatement(insertBuilding);
                buildingPreparedSt.setString(1, building.getBuildingID().toString());
                buildingPreparedSt.setString(2, building.getDescription());
                buildingPreparedSt.setString(3, building.getBuildingName());
                buildingPreparedSt.setString(4, building.getPhoneNumber());
                buildingPreparedSt.executeUpdate();

                System.out.println("BuildingDAO: *************** Inserting Address ");
                /** insert address */
                Address address = building.getAddress();
                String insertAddress = "INSERT INTO addresses (streetlineone, streetlinetwo, city, state, zip, country, buildingid) VALUES (?, ?, ?, ?, ?, ?, ?);";
                addressPreparedSt = connection.prepareStatement(insertAddress);
                addressPreparedSt.setString(1, address.getStreetLineOne());
                addressPreparedSt.setString(2, address.getStreetLineTwo());
                addressPreparedSt.setString(3, address.getCity());
                addressPreparedSt.setString(4, address.getState());
                addressPreparedSt.setString(5, address.getZip());
                addressPreparedSt.setString(6, address.getCountry());
                addressPreparedSt.setString(7,building.getBuildingID().toString());
                addressPreparedSt.executeUpdate();


            } catch (SQLException ex) {
                System.out.println("Database Exception State: " + ex.getSQLState());
                System.out.println(ex.getMessage());
            } finally {

                try {
                    if (addressPreparedSt != null) {
                        addressPreparedSt.close();
                        buildingPreparedSt.close();
                    }
                    connection.close();


                } catch (SQLException ex) {
                    System.err.println("BuildingDAO: Threw a SQLException saving the building object.");
                    System.err.println(ex.getMessage());
                }
            }

        }
        else{
            System.out.println("Having trouble connecting to the database, sorry.");
        }

    }


}
