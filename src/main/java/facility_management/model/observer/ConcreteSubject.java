package facility_management.model.observer;

import java.util.ArrayList;
import java.util.List;

public class ConcreteSubject implements Subject {

    List<facility_management.model.observer.Observer> observers;
    boolean updated;
    boolean status;

    public ConcreteSubject(){
        observers = new ArrayList<>();
    }

    @Override
    public void attach(Observer observer) {
        if(observer != null && observers.contains(observer) != true) {
            observers.add(observer);
        }
    }

    @Override
    public void detach(Observer observer ) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        List<Observer> temp = null;

        if (updated == false) {
            /** Define temp before we go through in case something is added while this is happening */
            temp = observers;
            for (int i = 0; i < observers.size(); i++) {
                observers.get(i).updateObserver();
            }
        }
    }

    @Override
    public boolean getState(facility_management.model.observer.Observer observer){
        return status;
    }

    /**
     * Based on whether maintenance order boolean is true or false. So change when the status changes.
     * @param resolved
     */
    public void setOrderResolved(boolean resolved){
        System.out.println("Maintenance order is resolved: " + resolved);
        status = resolved;
        updated = true;
        notifyObservers();
    }

}
