package facility_management.model.observer;

import java.util.Observer;

/**
 * This interface is based on class powerpoint notes and the following article https://www.journaldev.com/1739/observer-design-pattern-in-java
 */

public interface Subject {

    public void attach(facility_management.model.observer.Observer observer);
    public void detach(facility_management.model.observer.Observer observer);
    public void notifyObservers(); // can't name notify because of notify() in Java's object class
    public boolean getState(facility_management.model.observer.Observer observer);
}
