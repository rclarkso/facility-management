package facility_management.model.observer;

public class ConcreteObserver implements Observer {

    Subject subject;

    @Override
    public void updateObserver() {
        boolean status = subject.getState(this);
        System.out.println("Observer status: " + status);
    }

    @Override
    public void setSubject(Subject subject) {
        this.subject = subject;
    }


}
