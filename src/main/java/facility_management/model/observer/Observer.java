package facility_management.model.observer;

/**
 * This interface is based on class powerpoint notes and the following article https://www.journaldev.com/1739/observer-design-pattern-in-java
 */

public interface Observer {
    public void updateObserver();
    public void setSubject(Subject subject);
}
