package facility_management.model.maintenance;

public class PlumbingType implements RequestType {
    String type;
    public PlumbingType(){
        type = "PLUMBING";
    }

    @Override
    public void applyType() {
        System.out.println("Plumbing type applied");
    }
}
