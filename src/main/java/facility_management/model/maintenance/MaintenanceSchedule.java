package facility_management.model.maintenance;

import java.sql.Timestamp;
import java.util.UUID;

public class MaintenanceSchedule {

    Timestamp startTime;
    Timestamp endTime;
    UUID scheduleID;

    public MaintenanceSchedule(){
        scheduleID = UUID.randomUUID();
        startTime = null;
        endTime = null;
    }

    public MaintenanceSchedule(UUID scheduleID, Timestamp startTime, Timestamp endTime) {
        this.scheduleID = scheduleID;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public UUID getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(UUID scheduleID) {
        this.scheduleID = scheduleID;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public void printInformation(){
        if (startTime != null && endTime != null){
            System.out.println("Start Time: " + startTime);
            System.out.println("End Time: " + endTime);
        }
        System.out.println("Schedule ID: " + scheduleID.toString());
    }

}
