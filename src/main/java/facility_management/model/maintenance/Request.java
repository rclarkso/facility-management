package facility_management.model.maintenance;

import java.sql.Timestamp;
import java.util.UUID;

public class Request extends AbstractRequest {


    public Request() {
        //spring should make the order obj

    }

    public Request(RequestType type, Order newOrder, Timestamp timeOfRequest, String issue, UUID requestID){
        super(type,newOrder,timeOfRequest,issue,requestID);
    }

    @Override
    public void applyType(){
        System.out.println("Request has type applied: " + type);
        type.applyType();
    }

    public void printInformation(){
        System.out.println("\nRequest ID: " + requestID);
        System.out.println("Issue: " + issue);
        System.out.println("Time of Request: " + timeOfRequest);
        newOrder.printInformation();
    }
}
