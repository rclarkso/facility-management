package facility_management.model.maintenance;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;

public class Order {

    Timestamp completed;
    BigDecimal estimatedCost;
    UUID orderID;
    boolean resolved;
    MaintenanceSchedule scheduledTime;

    public Order(){
        completed = null;
        estimatedCost = BigDecimal.valueOf(50 + (int)(Math.random() * ((500-50) + 1)));
        orderID = UUID.randomUUID();
        resolved = false;
        // spring should make the maint schedule object
    }


    public MaintenanceSchedule getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(MaintenanceSchedule scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public Timestamp getCompleted() {
        return completed;
    }

    public void setCompleted(Timestamp completed) {
        this.completed = completed;
    }

    public BigDecimal getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(BigDecimal estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public UUID getOrderID() {
        return orderID;
    }

    public void setOrderID(UUID orderID) {
        this.orderID = orderID;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public void printInformation(){
        if (completed!=null){
            System.out.println("Time completed: " + completed);
        }
        System.out.println("Estimated Cost: " + estimatedCost);
        System.out.println("Order ID: " + orderID.toString());
        System.out.println("Issue Resolved: " + resolved);
        scheduledTime.printInformation();
    }

}
