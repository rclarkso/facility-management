package facility_management.model.maintenance;

import java.sql.Timestamp;
import java.util.UUID;

public abstract class AbstractRequest {
    RequestType type;
    Order newOrder;
    Timestamp timeOfRequest;
    String issue;
    UUID requestID;

    /** For bridge pattern */
    public AbstractRequest(RequestType type, Order newOrder, Timestamp timeOfRequest, String issue, UUID requestID){
        this.newOrder = newOrder;
        this.timeOfRequest = timeOfRequest;
        this.issue = issue;
        this.requestID = requestID;
        this.type = type;
    }

    public AbstractRequest(){};

    public void applyType(){
        System.out.println("Bridge pattern - request type applied.");
    }

    public Order getNewOrder() {
        return newOrder;
    }

    public void setNewOrder(Order newOrder) {
        this.newOrder = newOrder;
    }

    public UUID getRequestID() {
        return requestID;
    }

    public void setRequestID(UUID requestID) {
        this.requestID = requestID;
    }

    public Timestamp getTimeOfRequest() {
        return timeOfRequest;
    }

    public void setTimeOfRequest(Timestamp timeOfRequest) {
        this.timeOfRequest = timeOfRequest;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

}
