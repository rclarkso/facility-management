package facility_management.model.maintenance;

import java.util.List;

/**
 * Class that can be used to retrieve list of active/inactive facility_management.dal.model.maintenance orders.
 */
public class MaintenanceLog {

    List<MaintenanceSchedule> completedOrders;
    List<MaintenanceSchedule> activeOrders;


    public MaintenanceLog() {
        // spring should generate the obj lists
    }

    public List<MaintenanceSchedule> getCompletedOrders() {
        return completedOrders;
    }

    public void setCompletedOrders(List<MaintenanceSchedule> completedOrders) {
        this.completedOrders = completedOrders;
    }

    public List<MaintenanceSchedule> getActiveOrders() {
        return activeOrders;
    }

    public void setActiveOrders(List<MaintenanceSchedule> activeOrders) {
        this.activeOrders = activeOrders;
    }
}
