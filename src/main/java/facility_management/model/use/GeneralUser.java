package facility_management.model.use;

import java.util.UUID;

public class GeneralUser extends User {

    private UUID roomID;

    public GeneralUser(){

    }

    public void setUser(User user) {
        super.setType(user.getType());
        super.setUserID(user.getUserID());
        super.setFirstName(user.getFirstName());
        super.setLastName(user.getLastName());
        super.setEmail(user.getEmail());
        super.setPhoneNumber(user.getPhoneNumber());
    }

    public UUID getRoomID() {
        return roomID;
    }

    public void setRoomID(UUID roomID) {
        this.roomID = roomID;
    }



}
