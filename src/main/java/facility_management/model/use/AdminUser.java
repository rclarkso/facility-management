package facility_management.model.use;

public class AdminUser extends User {


    public AdminUser(){

    }

    public void setUser(User user) {
        super.setType(user.getType());
        super.setUserID(user.getUserID());
        super.setFirstName(user.getFirstName());
        super.setLastName(user.getLastName());
        super.setEmail(user.getEmail());
        super.setPhoneNumber(user.getPhoneNumber());
    }

}
