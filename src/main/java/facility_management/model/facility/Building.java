package facility_management.model.facility;

import java.util.List;
import java.util.UUID;

/**
 * Facilities contain rooms
 */
public class Building {
    List<Room> rooms;

    /** general facility_management.dal.model.maintenance.facility information */
    String buildingName;
    Address address;
    String phoneNumber;
    String description;
    UUID buildingID;
    int buildingCapacity;

    /** TODO: things we need to add global variables for but don't understand: listInspections, actualUsage, usageRate */

    public Building(){
        // spring should generate the address and rooms list objects
    }

    public Building(List<Room> rooms, String buildingName, Address address, String phoneNumber, String description, UUID buildingID) {
        this.rooms = rooms;
        this.buildingName = buildingName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.buildingID = buildingID;
    }

    public Building(String name,String phoneNumber, String description) {
        this.buildingName = name;
        this.phoneNumber = phoneNumber;
        this.description = description;
    }

    /**
     * Getters & setters
     */
    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> facilityRooms) {
        this.rooms = facilityRooms;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getBuildingID() {
        return buildingID;
    }

    public void setBuildingID(UUID buildingID) {
        this.buildingID = buildingID;
    }

    public int getBuildingCapacity(){
        int total = 0;

        for (Room r : rooms){
            total += r.getCapacity();
        }

        return total;
    }

    public void printInformation(){
        System.out.println("BUILDING ID: " + buildingID);
        System.out.println("BUILDING NAME: " + buildingName);
        System.out.println("BUILDING PHONE: " + phoneNumber);
        System.out.println("BUILDING DESCRIPTION: " + description);
        System.out.println("BUILDING CAPACITY: " + getBuildingCapacity());

        System.out.println("\n -- BUILDING ADDRESS -- ");
        address.printAddress();

    }


}
