package facility_management.model.facility;

public class Address {

    public String streetLineOne;
    public String streetLineTwo;
    public String city;
    public String state;
    public String zip;
    public String country;

    public Address(){

    }

    public Address(String streetLineOne, String streetLineTwo, String city, String state, String zip, String country) {
        this.streetLineOne = streetLineOne;
        this.streetLineTwo = streetLineTwo;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.country = country;
    }



    public String getStreetLineOne() {
        return streetLineOne;
    }

    public void setStreetLineOne(String streetLineOne) {
        this.streetLineOne = streetLineOne;
    }

    public String getStreetLineTwo() {
        return streetLineTwo;
    }

    public void setStreetLineTwo(String streetLineTwo) {
        this.streetLineTwo = streetLineTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void printAddress(){
        System.out.println("Street Line One: " + streetLineOne);
        System.out.println("Street Line Two: " + streetLineTwo);
        System.out.println("City: " + city);
        System.out.println("State: " + state);
        System.out.println("Zip: " + zip);
        System.out.println("Country: " + country);

    }
}
