package facility_management.model.facility;

import java.util.UUID;

/**
 * Room is the smallest building unit.
 */
public class Room {

    UUID roomID;
    UUID buildingID;
    int capacity;
    int floor;
    int unit;
    RoomStatus roomStatus;
    String rent;

    public enum RoomStatus {
        VACANT, OCCUPIED
    }

    public Room(){
    }

    public Room(int capacity, UUID roomID, int unit, UUID buildingID, RoomStatus roomStatus, String rent){
        this.buildingID = buildingID;
        this.capacity = capacity;
        this.roomID = roomID;
        this.unit = unit;
        this.roomStatus = roomStatus;
        this.rent = rent;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public UUID getRoomID() {
        return roomID;
    }

    public void setRoomID(UUID roomID) {
        this.roomID = roomID;
    }

    public UUID getBuildingID() {
        return buildingID;
    }

    public void setBuildingID(UUID buildingID) {
        this.buildingID = buildingID;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public RoomStatus getRoomStatus() {
        return roomStatus;
    }

    public void setRoomStatus(RoomStatus roomStatus) {
        this.roomStatus = roomStatus;
    }

    public void printInformation(){
        System.out.println("ROOM ID: " + roomID);
        System.out.println("BUILDING ID: " + buildingID);
        System.out.println("ROOM UNIT: " + unit);
        System.out.println("ROOM FLOOR: " + floor);
        System.out.println("ROOM CAPACITY: " + capacity);
        System.out.println("ROOM STATUS: " + roomStatus.toString());
        System.out.println("ROOM RENT: " + rent);
    }

}
