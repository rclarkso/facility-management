package facility_management.model.facility;

import facility_management.model.use.GeneralUser;

import java.sql.Date;
import java.util.UUID;

public class Lease  {

    GeneralUser user;
    Room room;
    Date startDate;
    Date endDate;
    String rent;
    UUID leaseID;

    public Lease(){
        // spring should generate the user obj and the room obj
    }


    public Lease(Room room, GeneralUser user, Date startDate, Date endDate, UUID leaseID) {
        this.room = room;
        this.user = user;
        this.startDate = startDate;
        this.endDate = endDate;
        rent = room.getRent();
        this.leaseID = leaseID;
    }


    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public UUID getLeaseID() {
        return leaseID;
    }

    public void setLeaseID(UUID leaseID) {
        this.leaseID = leaseID;
    }

    public GeneralUser getUser() {
        return user;
    }

    public void setUser(GeneralUser user) {
        this.user = user;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getRent() {
        return rent;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    public void printInformation(){
        System.out.println("Start Date: " + startDate);
        System.out.println("End Date: " + startDate);
        System.out.println("Rent: " + rent);
        System.out.println("Lease ID: " + leaseID);
        System.out.println("Room ID: " + room.getRoomID());
        System.out.println("User ID: " + user.getUserID());

    }
}
