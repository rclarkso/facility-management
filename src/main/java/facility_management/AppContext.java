package facility_management;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This singleton class allows the spring context to be set once and then accessed from the multiple interfaces/views.
 */
public class AppContext {

    static ApplicationContext context = null;

    protected AppContext(){}

    public static ApplicationContext getContext(){
        if (context == null){
            return new ClassPathXmlApplicationContext("application-context.xml");
        }
        else{
            return context;
        }
    }

}
