package facility_management;

import java.sql.*;

/**
 * This class was taken from the BookStore example given to students through class.
 * Used because we have very little DB experience!
 */
public class DBHelper {

    public static Connection getConnection() {

        System.out.println("DBHelper: -------- PostgreSQL " + "JDBC Connection  ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("DBHelper: Check Where  your PostgreSQL JDBC Driver exist and " + "Include in your library path!");
            e.printStackTrace();
            return null;

        }

        System.out.println("DBHelper: PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {
            String connectionURL = "jdbc:postgresql://ec2-54-235-64-195.compute-1.amazonaws.com:5432/daa8dm95equsme?user=wsrnaabnaoqxjt&password=0f6527f468f82a8c697ea3df990440ef23aa2faca1316f4b91cd98b0b9f6c627&ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory";
            connection = DriverManager.getConnection(connectionURL);
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT VERSION()");

            if (rs.next()) {
                System.out.println("DBHelper: The Database Version is " + rs.getString(1));
            }

        } catch (SQLException e) {

            System.out.println("DBHelper: Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }

        if (connection != null) {
            System.out.println("DBHelper: You have a database connection!");
        } else {
            System.out.println("DBHelper: Failed to make connection!");
        }

        return connection;
    }
}
